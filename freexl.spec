Name:           freexl
Version:        2.0.0
Release:        1
Summary:        Library for extracting data from Excel tables 
License:        MPLv1.1 or GPLv2+ or LGPLv2+
URL:            http://www.gaia-gis.it/FreeXL
Source0:        http://www.gaia-gis.it/gaia-sins/%{name}-sources/%{name}-%{version}.tar.gz
BuildRequires:  gcc doxygen minizip-devel expat-devel

%description
FreeXL is a library for extracting valid data from Excel spreadsheets (.xls)

Design goals:
    * simple and lightweight
    * stable, robust and efficient
    * easily and universally portable
    * completely ignore any GUI-related oddity

%package devel
Summary:        Development Libraries for FreeXL
Requires:       %{name} = %{version}-%{release} pkgconfig

%description devel
The %{name}-devel package is used for development libraries and header files.

%prep
%autosetup -p1

%build
%configure --enable-gcov=no --disable-static
%make_build

sed -i 's/^INSTALL_HEADER = \$(INSTALL_DATA)/& -p/' headers/Makefile.in
doxygen
rm -f html/installdox

%check
make check
cd  examples
  make clean
cd -

%install
%make_install
rm -f %{buildroot}%{_libdir}/lib%{name}.la
rm -rf examples/.libs/

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc COPYING AUTHORS README
%{_libdir}/lib%{name}.so.*

%files devel
%doc examples html
%{_includedir}/freexl.h
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/freexl.pc

%changelog
* Fri Sep 15 2023 yaoxin <yao_xin001@hoperun.com> - 2.0.0-1
- Update to 2.0.0

* Thu Sep 09 2021 lingsheng <lingsheng@huawei.com> - 1.0.5-4
- Remove .libs directory before packaging

* Wed Mar 04 2020 yangjian<yangjian79@huawei.com> - 1.0.5-3
- Package init
